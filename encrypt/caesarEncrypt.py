import argparse

# *** USAGE ***
# python caesarEncrypt plain_text.txt shift
# all shifts are taken to be rights shifts, e.g a shift of 5 would turn A to F.
# prints results to terminal
# can be written to file using redirection e.g > output_file.txt
parser = argparse.ArgumentParser(description = 'read the plain text to be encrypted.')

parser.add_argument("plain_text", help = "a file containing the plain text")
parser.add_argument("shift", help = "the amount to right shift the plain text", type = int)

args = parser.parse_args()

# read the plain_text from the file
file = open(args.plain_text, "r")
plain_text = file.read()
file.close()

# convert to all lowercase and split into a list for speed
plain_text_l = plain_text.replace(' ','').lower()
cipher_text = list(plain_text_l)

for i in range(0, len(cipher_text)):
    # get the letters offset from a and add the shift to it, then find its modulus of 26, set this to be its new offset and convert back to a char
    letter_offset = ((ord(cipher_text[i]) -97 + args.shift) % 26)
    cipher_text[i] = chr(letter_offset + 97)

print ''.join(cipher_text).upper()
