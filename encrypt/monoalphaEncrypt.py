import argparse

# *** USAGE ***
# python monoalphaEncrypt plain_text.txt key.txt.
# prints results to terminal.
# can be written to file using redirection e.g > output_file.txt.
parser = argparse.ArgumentParser(description = 'encrypts plain text using monoalphabetic substitution.')

parser.add_argument("plain_text", help = "a file containing the plain text")
parser.add_argument("key", help = "a file containing a 26 character cipher alphabet")

args = parser.parse_args()

# read the plain_text from the file.
file = open(args.plain_text, "r")
plain_text = file.read()
file.close()

# read the key from the cipher_key text file.
file_key = open(args.key, "r")
key_string = file_key.read()
file_key.close()

# convert to all lowercase and split into a list for speed.
#replace(' ','')
plain_text_l = plain_text.lower()
cipher_text = list(plain_text_l)

cipher_key = {  'a':'',
                'b':'',
                'c':'',
                'd':'',
                'e':'',
                'f':'',
                'g':'',
                'h':'',
                'i':'',
                'j':'',
                'k':'',
                'l':'',
                'm':'',
                'n':'',
                'o':'',
                'p':'',
                'q':'',
                'r':'',
                's':'',
                't':'',
                'u':'',
                'v':'',
                'w':'',
                'x':'',
                'y':'',
                'z':'',
                ' ':' '
            }

#split the key into a list by each character, check the length is 26 chars.
# remove all extra chars in the key, e.g \n etc.
key = list(key_string.replace('\n',''))

if (len(key) != 26):
    # halt execution, cannot correctly encrypt the plain text without the correct cipher alphabet.
    print 'ERROR, incorrect cipher alphabet length' + str(len(key))
else:
    # store the key inside the dictionary.
    for q in range(len(key)):
        cipher_key.update({str(chr(97 + q)): key[q]})

for i in range(0, len(cipher_text)):
    # swap each letter for its cipher alphabet equivalent.
    cipher_text[i] = str(cipher_key.get(cipher_text[i]))

print ''.join(cipher_text)
