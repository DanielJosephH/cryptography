import argparse

# *** USAGE ***
# python vigenereEncrypt.py plain_text.txt key.txt.
# prints results to terminal.
# can be written to file using redirection e.g > output_file.txt.
parser = argparse.ArgumentParser(description = 'encrypts plain text using vigenere square substitution.')

parser.add_argument("plain_text", help = "a file containing the plain text")
parser.add_argument("key", help = "a file containing a keyword for the vigenere square")

args = parser.parse_args()

# read the plain_text from the file.
file = open(args.plain_text, "r")
plain_text = file.read()
file.close()

# read the key from the cipher_key text file.
file_key = open(args.key, "r")
key_string = file_key.read()
key = key_string.lower().replace('\n','')
file_key.close()

# convert to all lowercase and split into a list for speed.
plain_text_l = plain_text.lower()
cipher_text = list(plain_text_l)

# encrypt each letter using a caesar cipher determined by the keyword.
for i in range(0, len(cipher_text)):
    # if keyword length is less than plain_text length then repeat keyword.
    shift = (ord(key[i % len(key)]) - 97)
    # get the letters offset from a and add the shift to it, then find its modulus of 26, set this to be its new offset and convert back to a char
    letter_offset = ((ord(cipher_text[i]) - 97 + shift) % 26)
    cipher_text[i] = chr(letter_offset + 97)

print ''.join(cipher_text).upper()
