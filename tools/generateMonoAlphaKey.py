import argparse
from collections import OrderedDict
import random

# *** USAGE ***
# python generateMonoAlphaKey.py keyword [-r].
# prints results to terminal.
# can be written to file using redirection e.g > output_file.txt.
parser = argparse.ArgumentParser(description = 'generates a cipher alphabet using a keyword.')

parser.add_argument("keyword", help = "a keyword to use to generate the cipher alphabet")
parser.add_argument("-r", "--randomize", help = "randomize the remaining letters of the alphabet", action = "store_true")

args = parser.parse_args()

# remove any repeated chars from the keyword
key_word_chars = list(OrderedDict.fromkeys(args.keyword))

# if the -r option is not given, then generate the rest of the key alphabetically, starting from the last char
# else, randomize the rest of the chars
if args.randomize:
    # add the entire alphabet on the end of the key in random order
    alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
    random.shuffle(alphabet)
    key_word_chars.extend(alphabet)
else:
    # get the ascii value of the last char and start alphabetically from there
    last_char = key_word_chars[len(key_word_chars) - 1]

    for i in range(0, 25):
        next_char = chr((ord(last_char) - 97 + 1) % 26 + 97)
        key_word_chars.append(next_char)
        last_char = next_char

# remove repeats from cipher alphabet
cipher_chars = list(OrderedDict.fromkeys(key_word_chars))
cipher_alphabet = ''.join(cipher_chars).upper()
print cipher_alphabet
