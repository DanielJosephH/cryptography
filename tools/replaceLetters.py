import argparse

# *** USAGE ***
# python monoalphaEncrypt cipher_text.txt key.txt.
# prints results to terminal.
# can be written to file using redirection e.g > output_file.txt.
parser = argparse.ArgumentParser(description = 'decrypts cipher text using monoalphabetic substitution.')

parser.add_argument("cipher_text", help = "a file containing the cipher text")
parser.add_argument("key", help = "a file containing a 26 character cipher alphabet")

args = parser.parse_args()

# read the cipher_text from the file.
file = open(args.cipher_text, "r")
cipher_text = file.read()
file.close

# read the key from the cipher_key text file.
file_key = open(args.key, "r")
key_string = file_key.read()
file_key.close

# convert to all lowercase and split into a list for speed.
plain_text = list(cipher_text)

cipher_key = {
                ' ':' '
            }

#split the key into a list by each character, check the length is 26 chars.
# remove all extra chars in the key, e.g \n etc.
key = key_string.replace('\n','')

# split by comma to get replacement pairs then store the pairs in the dictionary.
key_pairs = key.split(",")
for i in range(len(key_pairs)):
    # split by the ':' and store the cipher letter as the key for easy look up in cipher text.
    parts = key_pairs[i].split(":")
    # A:blank symbolizes that the letter should not be replaced
    if len(parts[1]) != 0:
        cipher_key.update({parts[0]:parts[1]})
    else:
        cipher_key.update({parts[0]:parts[0]})

# replace letters that appear in cipher text with their resepctive letters
for q in range(len(cipher_text)):
    if cipher_key.get(cipher_text[q]):
        plain_text[q] = cipher_key.get(cipher_text[q])

print ''.join(plain_text)
