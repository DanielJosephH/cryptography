import argparse

# *** USAGE ***
# python analyseLetterFrequency.py cipher_text.txt
# prints results to terminal.
# can be written to file using redirection e.g > output_file.txt.
parser = argparse.ArgumentParser(description = 'creates a table showing the relative frequencies of each letter in the cipher text.')

parser.add_argument("cipher_text", help = "the cipher text to be analysed")

args = parser.parse_args()

# get the cipher text from the file
file = open(args.cipher_text, "r")
cipher_text = file.read()
file.close()

cipher_chars = {'A':0,
                'B':0,
                'C':0,
                'D':0,
                'E':0,
                'F':0,
                'G':0,
                'H':0,
                'I':0,
                'J':0,
                'K':0,
                'L':0,
                'M':0,
                'N':0,
                'O':0,
                'P':0,
                'Q':0,
                'R':0,
                'S':0,
                'T':0,
                'U':0,
                'V':0,
                'W':0,
                'X':0,
                'Y':0,
                'Z':0
            }

# split cipher_text into chars
cipher_text_chars = list(cipher_text)

for i in range(0,26):
    # if a symbol appears that is in the dictionary, then increase its count
    cipher_chars.update({chr(65 + i): cipher_text_chars.count(chr(65 + i))})

# count the total number of symbols in the cipher_text and print the results as a relative frequency
total_chars = 0
for q in list(cipher_chars.values()):
    total_chars += q

# replace each dict value with a percentage frequency
for k in cipher_chars:
    cipher_chars.update({k:(float(cipher_chars.get(k))/total_chars) * 100})
    # print the results in an understandable format
    print k + ': ' + "{0:.2f}".format(cipher_chars.get(k)) + '%'
