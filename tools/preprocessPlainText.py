import argparse
import string

# *** USAGE ***
# python preprocessPlainText.py plain_text.txt -s
# prints results to terminal.
# can be written to file using redirection e.g > output_file.txt.
parser = argparse.ArgumentParser(description = 'removes punctuation and spaces from plain text file.')

parser.add_argument("plain_text", help = "the plain text to be pre-processed")
parser.add_argument("-s", "--spaces", help = "remove spaces", action = "store_true")

args = parser.parse_args()

# get the plain text from the file
file = open(args.plain_text, "r")
plain_text = file.read()
file.close()

# change to lower case
plain_text_l = plain_text.lower()

# remove punctuation
if args.spaces:
    plain_text_l_without_spaces = plain_text_l.replace(' ','')
    preprocessed_plain_text = plain_text_l_without_spaces.translate(None, string.punctuation)
else:
    preprocessed_plain_text = plain_text_l.translate(None, string.punctuation)

print preprocessed_plain_text
