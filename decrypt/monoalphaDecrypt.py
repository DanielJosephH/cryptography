import argparse

# *** USAGE ***
# python monoalphaEncrypt cipher_text.txt key.txt.
# prints results to terminal.
# can be written to file using redirection e.g > output_file.txt.
parser = argparse.ArgumentParser(description = 'decrypts cipher text using monoalphabetic substitution.')

parser.add_argument("cipher_text", help = "a file containing the cipher text")
parser.add_argument("key", help = "a file containing a 26 character cipher alphabet")

args = parser.parse_args()

# read the cipher_text from the file.
file = open(args.cipher_text, "r")
cipher_text = file.read()
file.close()

# read the key from the cipher_key text file.
file_key = open(args.key, "r")
key_string = file_key.read()
file_key.close()

# convert to all lowercase and split into a list for speed.
plain_text = list(cipher_text)

cipher_key = {
                ' ':' '
            }

#split the key into a list by each character, check the length is 26 chars.
# remove all extra chars in the key, e.g \n etc.
key = list(key_string.replace('\n',''))

if (len(key) != 26):
    # halt execution, cannot correctly encrypt the plain text without the correct cipher alphabet.
    print 'ERROR, incorrect cipher alphabet length' + str(len(key))
else:
    # store the key inside the dictionary.
    for q in range(len(key)):
        cipher_key.update({key[q]:str(chr(97 + q))})

for i in range(0, len(plain_text)):
    # swap each letter for its cipher alphabet equivalent, only if it is in the cipher alphabet
    if cipher_key.get(plain_text[i]):
        plain_text[i] = str(cipher_key.get(plain_text[i]))

print ''.join(plain_text)
