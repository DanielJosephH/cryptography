import argparse

# *** USAGE ***
# python caesarDecrypt encrypted_text.txt shift
# all shifts are taken to be left shifts, e.g a shift of 5 would turn F to A.
# prints results to terminal
# can be written to file using redirection e.g > output_file.txt
parser = argparse.ArgumentParser(description = 'read the encrypted text to be decrypted.')

parser.add_argument("cipher_text", help = "a file containing the encrypted text")
parser.add_argument("shift", help = "the amount to left shift the encrypted text", type = int)

args = parser.parse_args()

# read the encrypted_text from the file
file = open(args.cipher_text, "r")
cipher_text = file.read().lower()
file.close()

# split into a list for speed
plain_text = list(cipher_text)

for i in range(0, len(plain_text)):
    # get the letters offset from a and subtract the shift to it, then find its modulus of 26, set this to be its new offset and convert back to a char
    letter_offset = ((ord(plain_text[i]) -97 - args.shift) % 26)
    plain_text[i] = chr(letter_offset + 97)

print ''.join(plain_text)
