import argparse

# *** USAGE ***
# python vigenereDecrypt.py cipher_text.txt key.txt.
# prints results to terminal.
# can be written to file using redirection e.g > output_file.txt.
parser = argparse.ArgumentParser(description = 'decrypts cipher text using vigenere square substitution.')

parser.add_argument("cipher_text", help = "a file containing the cipher text")
parser.add_argument("key", help = "a file containing a keyword for the vigenere square")

args = parser.parse_args()

# read the cipher text from the file.
file = open(args.cipher_text, "r")
cipher_text = file.read()
file.close()

# read the key from the cipher_key text file.
file_key = open(args.key, "r")
key_string = file_key.read()
key = key_string.lower().replace('\n','')
file_key.close()

# convert to all lowercase and split into a list for speed.
plain_text_l = cipher_text.lower()
plain_text = list(plain_text_l)

# encrypt each letter using a caesar cipher determined by the keyword.
for i in range(0, len(plain_text)):
    # if keyword length is less than plain_text length then repeat keyword.
    shift = (ord(key[i % len(key)]) - 97)
    # moving back by n places is the same as shifting forward by 26 - n spaces.
    # get the letters offset from a and add the shift to it, then find its modulus of 26, set this to be its new offset and convert back to a char
    letter_offset = ((ord(plain_text[i]) - 97 + (26 - shift)) % 26)
    plain_text[i] = chr(letter_offset + 97)

print ''.join(plain_text).lower()
